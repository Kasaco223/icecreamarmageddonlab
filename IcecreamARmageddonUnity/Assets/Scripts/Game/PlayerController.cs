using System.Collections;
using UnityEngine;
using TMPro;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed = 1f; // Velocidad de movimiento del Player
    public GameObject projectilePrefab; // Prefab del proyectil
    public Transform projectileSpawnPoint; // Punto de spawn del proyectil
    public float projectileSpeed = 10f; // Velocidad del proyectil
    public TextMeshProUGUI livesText; // TextMeshPro para mostrar las vidas

    private int lives = 3; // Variable para las vidas
    public bool isInvulnerable = false;
    private Rigidbody rb;
    private Vector3 moveDirection;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        UpdateLivesText(); // Actualizar el texto de vidas al inicio
    }

    void Update()
    {
        // Disparar proyectil con el teclado num�rico
        if (Input.GetKeyDown(KeyCode.Keypad8))
        {
            ShootProjectile(Vector3.forward);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad4))
        {
            ShootProjectile(Vector3.left);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad2))
        {
            ShootProjectile(Vector3.back);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad6))
        {
            ShootProjectile(Vector3.right);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad7)) // Diagonal superior izquierda
        {
            ShootProjectile(Vector3.forward + Vector3.left);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad9)) // Diagonal superior derecha
        {
            ShootProjectile(Vector3.forward + Vector3.right);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad1)) // Diagonal inferior izquierda
        {
            ShootProjectile(Vector3.back + Vector3.left);
        }
        else if (Input.GetKeyDown(KeyCode.Keypad3)) // Diagonal inferior derecha
        {
            ShootProjectile(Vector3.back + Vector3.right);
        }

        // Obtener las entradas de movimiento
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        // Calcular la direcci�n de movimiento
        moveDirection = new Vector3(horizontalInput, 0f, verticalInput).normalized;

        // Set forces to zero if no key is pressed
        if (horizontalInput == 0 && verticalInput == 0)
        {
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
        }
    }


    void FixedUpdate()
    {
        // Mover al Player
        rb.MovePosition(rb.position + moveDirection * moveSpeed * Time.fixedDeltaTime / 2);
    }

    void ShootProjectile(Vector3 direction)
    {
        // Instanciar el proyectil
        GameObject projectile = Instantiate(projectilePrefab, projectileSpawnPoint.position, Quaternion.identity);
        Rigidbody projectileRb = projectile.GetComponent<Rigidbody>();

        // Aplicar velocidad al proyectil
        projectileRb.velocity = direction * projectileSpeed;
    }

    private IEnumerator ActivateInvulnerability(float duration)
    {
        isInvulnerable = true;
        yield return new WaitForSeconds(duration);
        isInvulnerable = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Enemy"))
        {
            if (isInvulnerable)
            {
                // Destruir el enemigo
                Destroy(collision.gameObject);
            }
            else
            {
                // Restar una vida
                lives--;
                UpdateLivesText(); // Actualizar el texto de vidas

                // Verificar si el jugador muri�
                if (lives <= 0)
                {
                    livesText.text = "Death"; // Mostrar "Death" en el TextMeshPro
                    // Aqu� podr�as a�adir acciones adicionales cuando el jugador muere
                    Destroy(gameObject);

                }
                else
                {
                    // Teletransportar al jugador a una posici�n segura
                    transform.position = new Vector3(0, 0.25f, 0);
                    StartCoroutine(ActivateInvulnerability(2f)); // Activar invulnerabilidad por 2 segundos
                }
            }
        }
    }

    private void UpdateLivesText()
    {
        livesText.text = "Vidas: " + lives.ToString(); // Actualizar el texto de vidas
    }
}
