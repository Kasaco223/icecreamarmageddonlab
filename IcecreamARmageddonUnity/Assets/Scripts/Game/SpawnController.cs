using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    private float waitTime = 0.25f;
    private float timer = 0.0f;

    private int probability;

    private GameObject enemyPrefab; // Prefab del enemigo
    public GameObject enemyPrefab1; // Prefab del enemigo
    public GameObject enemyPrefab2; // Prefab del enemigo
    public GameObject enemyPrefab3; // Prefab del enemigo

    private float minSpawnTime = 0.25f; // Tiempo m�nimo de generaci�n
    public static float maxSpawnTime = 5f; // Tiempo m�ximo de generaci�n

    private void Start()
    {
        enemyPrefab = enemyPrefab1;
        // Iniciar la generaci�n de enemigos
        StartCoroutine(SpawnEnemies());
    }
    public void Update()
    {
        if (maxSpawnTime < minSpawnTime)
        {
            maxSpawnTime = minSpawnTime +2f;
        }
        timer += Time.deltaTime;
        if (timer > waitTime)
        {
           probability = Random.Range(0, 9);
            if (probability <= 4) { enemyPrefab = enemyPrefab1; }else if(probability >=5 && probability <= 7) { enemyPrefab = enemyPrefab2; }else{ enemyPrefab = enemyPrefab3;}
            timer -= waitTime;
        }
    }

    private IEnumerator SpawnEnemies()
    {
        while (true)
        {
            // Calcular un tiempo aleatorio de generaci�n
            float spawnTime = Random.Range(minSpawnTime, maxSpawnTime);
            yield return new WaitForSeconds(spawnTime);

            // Calcular una posici�n aleatoria
            Vector3 spawnPosition = GetRandomSpawnPosition();

            // Instanciar el enemigo en la posici�n aleatoria
            Instantiate(enemyPrefab, spawnPosition, Quaternion.identity);
        }
    }

    private Vector3 GetRandomSpawnPosition()
    {
        // Definir las posibles posiciones de spawn
        Vector3[] spawnPositions = new Vector3[]
        {
            new Vector3(-4f, 0.25f, 0f),
            new Vector3(4f, 0.25f, 0f),
            new Vector3(0f, 0.25f, 4f),
            new Vector3(0f, 0.25f, -4f)
        };

        // Seleccionar una posici�n aleatoria
        int randomIndex = Random.Range(0, spawnPositions.Length);
        return spawnPositions[randomIndex];
    }
}
