using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshEnemy : MonoBehaviour
{
    private NavMeshAgent agent;
    private GameObject playerObject;
    private Vector3 playerPosition;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        playerObject = GameObject.FindGameObjectWithTag("Player");
        if (playerObject != null)
        {
            playerPosition = playerObject.transform.position;
            if (agent.isOnNavMesh) // Verifica si el agente est� en el NavMesh
            {
                agent.destination = playerPosition;
            }
            else
            {
                Debug.LogWarning("El agente no est� en el NavMesh.");
            }
        }
        else
        {
            Debug.LogWarning("No se encontr� ning�n objeto con el tag 'Player'.");
        }
    }

}
