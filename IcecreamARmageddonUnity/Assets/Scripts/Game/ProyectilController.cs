using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProyectilController : MonoBehaviour
{
    // Cambiar el m�todo OnCollisionEnter por OnTriggerEnter para que funcione con un objeto trigger
    void OnTriggerEnter(Collider other)
    {
        // Verificar si el objeto colisionado tiene la etiqueta "Enemy"
        if (other.CompareTag("Enemy"))
        {
            // Destruir el proyectil y el objeto con la etiqueta "Enemy"
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
        else if (!other.CompareTag("Player"))
        {
            // Si no es un enemigo ni el jugador, destruir solo el proyectil
            Destroy(gameObject);
        }
    }
}
