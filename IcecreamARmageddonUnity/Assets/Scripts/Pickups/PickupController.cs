using UnityEngine;

public class PickupController : MonoBehaviour
{
    public float amplitude = 0.5f; // Amplitud del movimiento arriba y abajo
    public float frequency = 1.0f; // Frecuencia del movimiento arriba y abajo
    public float freezeDuration = 5.0f; // Duraci�n para congelar a los enemigos despu�s de la colisi�n

    private Vector3 position;

    private void Start()
    {
        position = transform.position;
        // Destruye el objeto despu�s de 5 segundos
        Destroy(gameObject, 7f);
    }

    private void Update()
    {
        // Mueve el objeto arriba y abajo en el eje Z
        float newZ = position.z + amplitude * Mathf.Sin(2 * Mathf.PI * frequency * Time.time);
        transform.position = new Vector3(position.x, position.y, newZ);
    }
}
