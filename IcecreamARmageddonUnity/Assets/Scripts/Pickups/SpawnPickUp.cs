using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPickUp : MonoBehaviour
{
    private float waitTime = 1f;
    private float timer = 0.0f;


    private int probability;

    private GameObject pickUpPrefab; 
    public GameObject pickUpIce; 
    public GameObject pickUpKillAll; 
    public GameObject pickUpShield; 
    public GameObject pickUpVelocity; 

    public float minSpawnTime = 1f; // Tiempo m�nimo de generaci�n
    public float maxSpawnTime = 10f; // Tiempo m�ximo de generaci�n

    private void Start()
    {
        pickUpPrefab = pickUpIce;
        // Iniciar la generaci�n de enemigos
        StartCoroutine(SpawnEnemies());
    }
    public void Update()
    {
        timer += Time.deltaTime;
        if (timer > waitTime)
        {
           probability = Random.Range(0, 5);
            if (probability == 1) { 
                pickUpPrefab = pickUpIce; 
            }
            else if(probability == 2) { 
                pickUpPrefab = pickUpKillAll; }
            else if (probability == 3)
            {
                pickUpPrefab = pickUpShield;
            }
            else if (probability == 4)
            {
                pickUpPrefab = pickUpVelocity;
            }

            timer -= waitTime;
        }
    }

    private IEnumerator SpawnEnemies()
    {
        while (true)
        {
            // Calcular un tiempo aleatorio de generaci�n
            float spawnTime = Random.Range(minSpawnTime, maxSpawnTime);
            yield return new WaitForSeconds(spawnTime);

            // Calcular una posici�n aleatoria
            Vector3 spawnPosition = GetRandomSpawnPosition();

            // Instanciar el enemigo en la posici�n aleatoria
            Instantiate(pickUpPrefab, spawnPosition, Quaternion.identity);
        }
    }

    private Vector3 GetRandomSpawnPosition()
    {
        // Definir las posibles posiciones de spawn
        Vector3[] spawnPositions = new Vector3[]
        {
            new Vector3(-4f, 0.2f, 4f),
            new Vector3(-4f, 0.2f, -4f),
            new Vector3(4f, 0.2f, 4f),
            new Vector3(4f, 0.2f, -4f)
        };

        // Seleccionar una posici�n aleatoria
        int randomIndex = Random.Range(0, spawnPositions.Length);
        return spawnPositions[randomIndex];
    }
}
