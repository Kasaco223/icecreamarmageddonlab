using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IceController : MonoBehaviour
{

    public float freezeDuration;

    private bool isFrozen = false;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // Destruye el objeto pickup
            Destroy(gameObject);

            // Congela a los enemigos con la etiqueta "Enemy"
            StartCoroutine(FreezeEnemies());
        }
    }

    private IEnumerator FreezeEnemies()
    {
        if (!isFrozen)
        {
            isFrozen = true;

            // Encuentra todos los objetos con la etiqueta "Enemy"
            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

            foreach (GameObject enemy in enemies)
            {
                // Desactiva el NavMeshAgent para detener el movimiento hacia el jugador
                NavMeshAgent navMeshAgent = enemy.GetComponent<NavMeshAgent>();
                if (navMeshAgent != null)
                {
                    navMeshAgent.isStopped = true; // Detiene el movimiento
                }
            }

            // Espera la duraci�n especificada
            yield return new WaitForSeconds(freezeDuration);

            // Reactiva el NavMeshAgent y permite que los enemigos se muevan nuevamente
            foreach (GameObject enemy in enemies)
            {
                NavMeshAgent navMeshAgent = enemy.GetComponent<NavMeshAgent>();
                if (navMeshAgent != null)
                {
                    navMeshAgent.isStopped = false; // Reactiva el movimiento
                }
            }

            isFrozen = false;
        }
    }
}
