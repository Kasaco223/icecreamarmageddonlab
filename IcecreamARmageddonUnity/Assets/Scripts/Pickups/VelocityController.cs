using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VelocityController : MonoBehaviour
{
    private float speedModifier = 5f; // Modificador de velocidad
    private float securitySpeed = 0f;
    private void Start()
    {
        securitySpeed = speedModifier;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // Encuentra el componente PlayerController en el objeto del jugador
            PlayerController playerController = other.GetComponent<PlayerController>();
            if (playerController != null)
            {
                // Aumenta la velocidad durante 5 segundos
                playerController.StartCoroutine(ChangeSpeedForDuration(playerController, speedModifier, 5f));
            }

            // Destruye este objeto
            Destroy(gameObject);
        }
    }

    private IEnumerator ChangeSpeedForDuration(PlayerController playerController, float newSpeed, float duration)
    {
        // Guarda la velocidad original
        float originalMoveSpeed = playerController.moveSpeed;

        // Cambia la velocidad
        playerController.moveSpeed += newSpeed;

        // Espera durante la duraci�n especificada
        yield return new WaitForSeconds(duration);
        securitySpeed += 1f;
        // Restaura la velocidad original
        playerController.moveSpeed = securitySpeed;
       
    }
}
