using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldController : MonoBehaviour
{
    private float invulnerabilityDuration = 5f; // Duraci�n de la invulnerabilidad en segundos
    public GameObject shieldPrefab; // Prefab del escudo

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // Destruye este objeto
            Destroy(gameObject);

            // Activa la invulnerabilidad en el Player
            PlayerController playerController = other.GetComponent<PlayerController>();
            if (playerController != null)
            {
                playerController.StartCoroutine(ActivateInvulnerability(playerController));
            }

            // Instancia el objeto del escudo como hijo del Player
            if (shieldPrefab != null)
            {
                // Crea el escudo como hijo del Player
                GameObject shieldInstance = Instantiate(shieldPrefab, other.transform);
                // Destruye el escudo despu�s de la duraci�n de invulnerabilidad
                Destroy(shieldInstance, invulnerabilityDuration);
            }
        }
    }

    private IEnumerator ActivateInvulnerability(PlayerController playerController)
    {
        // Guarda la velocidad original del jugador
        float originalMoveSpeed = playerController.moveSpeed;

        // Activa la invulnerabilidad
        playerController.isInvulnerable = true;

        // Espera durante la duraci�n especificada
        yield return new WaitForSeconds(invulnerabilityDuration);

        // Desactiva la invulnerabilidad
        playerController.isInvulnerable = false;

        // Restaura la velocidad original
        playerController.moveSpeed = originalMoveSpeed;
    }
}
