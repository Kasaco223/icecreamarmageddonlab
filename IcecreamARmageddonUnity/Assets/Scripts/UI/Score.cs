using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Score : MonoBehaviour
{
    private static int score = 0; // Puntuaci�n inicial
    public TextMeshProUGUI scoreText; // Referencia al componente TextMeshProUGUI en la escena
    private string enemyTag = "Enemy"; // Etiqueta de los objetos enemigos

    void Start()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        foreach (GameObject enemy in enemies)
        {
            enemy.GetComponent<DestroyObserver>().InitializeScoreScript();
        }
        UpdateScoreText(); // Asegura que el texto de la puntuaci�n se actualice al inicio
    }

    // M�todo para aumentar la puntuaci�n cuando se destruye un enemigo
    public static void AddScore(int points)
    {
        score += points;
        UpdateScoreText();
    }

    // M�todo para actualizar el texto de la puntuaci�n en pantalla
    private static void UpdateScoreText()
    {
        // Asegura que la referencia scoreText no sea nula antes de actualizar el texto
        if (Instance != null && Instance.scoreText != null)
        {
            Instance.scoreText.text = "Score: " + score;
            SpawnController.maxSpawnTime -= 0.10f;
        }
    }

    // Variable est�tica para acceder a la instancia de Score
    private static Score instance;
    private static Score Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<Score>();
            }
            return instance;
        }
    }
}
