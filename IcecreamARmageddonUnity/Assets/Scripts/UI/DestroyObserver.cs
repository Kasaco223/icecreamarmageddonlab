using UnityEngine;

public class DestroyObserver : MonoBehaviour
{
    private void OnDestroy()
    {
        Score.AddScore(10);
    }

    public void InitializeScoreScript()
    {
        Score.AddScore(0); // Llama a esta funci�n para asegurar que Score est� inicializado
    }
}
